import React, {Dispatch, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom'
import {RootState} from '../../store/interface/storeInterface';
import * as actions from '../../store/action'

import Modal from '../../common/modal';
import Loading from '../../common/loading';
import Toaster from "../../common/toaster";
import ListTable from "../../common/listtable";
import listColumn from "./column";
import mockData from './mockData.json'
import {ValidateForm, ValidateInput} from "../../helper/validateForm";
import somethingFormRule from "./rules/somethingFormRule";


interface ISomething {
    email: string,
    password: string
}

const Landing: React.FC = () => {
    const dispatch = useDispatch<Dispatch<any>>()
    const {modalMode} = useSelector((state: RootState) => state.modalReducer)
    const {loadingMode} = useSelector((state: RootState) => state.loadingReducer)
    const [values, setValues] = useState<ISomething>({
        email: '',
        password: ''
    })
    const [errors, setErrors] = useState<any>('')
    const history = useHistory()
    const initiateModal = () => {
        dispatch(actions.openModal("open_simple_modal"))
    }
    const handleBlur = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.persist()
        const ee = ValidateInput(e.target.name,e.target.value,somethingFormRule)
        setErrors({
            ...errors,
            [e.target.name]: ee
        })
    }

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        e.persist()
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }
    const handleSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault()
        setErrors(ValidateForm(values, somethingFormRule))
    }

    return (
        <div>
            <Toaster/>
            <p>I am landing </p>
            {
                loadingMode === '' && <Loading/>
            }
            <button onClick={initiateModal}> Open modal</button>
            {
                modalMode === "open_simple_modal" &&
                <Modal>
                    <div className="header">This is modal</div>
                    <div className="header">This is not a modal</div>
                </Modal>

            }
            <button onClick={() => history.push('/admin')}> go to admin</button>

            <section className="something-form">
                <form action="" onSubmit={(e: React.ChangeEvent<HTMLFormElement>) => handleSubmit(e)}>
                    <div className="input-group">
                        <input type="text"
                               name={"email"}
                               placeholder={"your email "}
                               onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                   handleChange(e)
                               }}
                               onBlur={(e: React.ChangeEvent<HTMLInputElement>) => {
                                   handleBlur(e)
                               }}
                        />
                        {
                            errors.email && errors.email.map((error:string, index:number) => (
                                <div className={"error-text"} key={index}>{error}</div>
                                ))
                        }
                    </div>
                    <div className="input-group">
                        <input type="password"
                               name={"password"}
                               placeholder={"******* "}
                               onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                   handleChange(e)
                               }}
                               onBlur={(e: React.ChangeEvent<HTMLInputElement>) => {
                                   handleBlur(e)
                               }}
                        />
                        {
                            errors.password && errors.password.map((error:string, index:number) => (
                                <div className={"error-text"} key={index}>{error}</div>
                            ))
                        }
                    </div>
                    <button type="submit" className="btn primary">
                        Save
                    </button>
                </form>

            </section>
            <section>
                <div className="tablethe">
                    here is table
                </div>
                <div className="table-container">
                    <ListTable
                        columns={listColumn}
                        rows={mockData}
                        deleteAction={() => console.log('i am delete ')}
                    />
                </div>

            </section>

        </div>
    );
};

export default Landing;
